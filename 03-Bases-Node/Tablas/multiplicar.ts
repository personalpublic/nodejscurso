const fs = require('fs');


let crearArchivo = (base) => {
    return new Promise((resolve,rejects)=> {
        if(!Number(base))
        {
            rejects("No es un numero");
            return;
        }
        let data = '';
        for (let index = 0; index < 10; index++) {
            data += `${base} *  ${index} = ${base*index} \n`;
            fs.writeFileSync('archivo.txt', data, (err) => {
                if (err) throw err;
                console.log("File creado");
            });
        }
    });
}