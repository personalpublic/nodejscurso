let empleados = [{
        id: 1,
        nombre: 'Fernando'
    },
    {
        id: 2,
        nombre: 'Maria'
    },
    {
        id: 3,
        nombre: 'Jose'
    }
];

let salarios = [{
        id: 1,
        salario: 1000
    },
    {
        id: 2,
        salario: 1000
    }

];

let getEmpleado = (id, callback) => {
    let empleadoDB = empleados.find(empleado => {
        return empleado.id === id;
    });
    if (!empleadoDB) {
        callback(`No existe ${id}`);
    } else {
        console.log(empleadoDB);
        callback(null, empleadoDB);
    }
};


let getSalario = (id, callback) => {
    let salarioDB = salarios.find(salario => {
        return salario.id === id;
    });
    if (!salarioDB) {
        callback(`No existe salario ${id}`);
    } else {
        console.log(salarioDB);
        callback(null, salarioDB);
    }
};

let getSalarioEmpleado = (id, callback) => {
    let empleadoDB = empleados.find(empleado => {
        return empleado.id === id;
    });
    if (empleadoDB) {
        let salarioDB = salarios.find(salario => {
            return salario.id === id;
        });
        if (salarioDB) {
            callback({
                nombre: empleadoDB.nombre,
                salario: salarioDB.salario
            });
            console.log(`Nombre: ${empleadoDB.nombre}`);
            console.log(`Salario: ${salarioDB.salario}`);
        } else {
            callback(`No existe Empleado con referencia al salario ${id}`);
        }
    } else {
        callback(`No existe Empleado con referencia al salario ${id}`);
    }
};

getSalario(1, (err, salarios) => {
    if (err) { console.log(err); } else {
        getEmpleado(1, (err, empleado) => {
            if (err) {
                console.log(err);
            }
        });
    }
});

getSalarioEmpleado(2, (err, salarios) => {
    if (err) {
        console.log(err);
    }
});