let deadpool = {
    nombre: 'Wade',
    apellido: 'Winston',
    poder: 'Regeneracion',
    getNombre: function() {
        return `${ this.nombre} ${ this.apellido} - poder: ${ this.poder}`;
    }
}

function sumar(a, b) {
    return a + b;
}
console.log(sumar(10, 20));

let sumarValores = (a, b) => {
    return a + b;
}
let sumarValoresCorto = (a, b) => a + b;

console.log(sumarValores(10, 20));
console.log(sumarValoresCorto(10, 30));
console.log(deadpool.getNombre());