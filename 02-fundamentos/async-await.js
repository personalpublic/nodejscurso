let getNombre = async() => {
    return 'Fernando';
};
console.log(getNombre());

getNombre().then((result) => {
    console.log(result);
}).catch((err) => {
    console.log(err);
});


let saludo = async() => {
    let nombre = await getNombre();
    return `Hola ${nombre}`;
};

saludo().then(mensaje => {
    console.log(mensaje);
});